from datetime import datetime
from dataclasses import dataclass, fields, is_dataclass, MISSING
from typing import get_origin, get_args, List

@dataclass
class Comment:
    user: str
    date: str
    text: str

@dataclass
class Issue:
    number: int
    user: str
    date: str
    title: str
    text: str
    comments: List[Comment]

@dataclass
class Repository:
    name: str
    issues: List[Issue]


def dataclass_from_dict(klass, dikt):
    if get_origin(klass) == list:
        assert isinstance(dikt, list)
        arg = get_args(klass)[0]
        return [dataclass_from_dict(arg, x) for x in dikt]
    if is_dataclass(klass) and isinstance(dikt, dict):
        fieldtypes = {}
        #fieldtypes = {f.name:f.type for f in fields(klass)}
        for f in fields(klass):
            if f.name not in dikt and f.default is MISSING:
                raise Exception("The field '%s' is required in '%s'" % \
                        (f.name, dikt))
            fieldtypes[f.name] = f.type
        args = {}
        for f in dikt:
            if not f in fieldtypes:
                raise Exception("The field '%s' is not declared " % f)
            args[f] = dataclass_from_dict(fieldtypes[f],dikt[f])
        return klass(**args)
    else:
        if isinstance(dikt, klass):
            return dikt
        else:
            raise Exception("The item '%s' is not of type '%s'" % \
                    (dikt, klass))

def dataclass_to_dict(dc):
    if isinstance(dc, (int, str)):
        return dc
    elif isinstance(dc, datetime):
        return str(dc)
    elif isinstance(dc, list):
        return [dataclass_to_dict(x) for x in dc]
    elif is_dataclass(dc):
        d = dc.__dict__.copy()
        assert isinstance(d, dict)
        for s in d:
            assert isinstance(s, str)
            d[s] = dataclass_to_dict(d[s])
        return d
    else:
        raise Exception("Not implemented")

def load_toml(filename, klass):
    import toml
    d = toml.load(open(filename))
    return dataclass_from_dict(klass, d)

def load_json(filename, klass):
    import json
    d = json.load(open(filename))
    return dataclass_from_dict(klass, d)

def save_json(filename, d):
    import json
    d = json.dump(dataclass_to_dict(d), open(filename, "w"), indent=4)

