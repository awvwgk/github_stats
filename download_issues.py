from dataclasses import dataclass

from github3 import login

from model import Repository, Issue, Comment, load_toml, save_json

@dataclass
class GitHub:
    user: str
    token: str

@dataclass
class Account:
    github: GitHub


account = load_toml("account.toml", Account)
user = account.github.user
pw = account.github.token

gh = login(user, pw)
repo = gh.repository("j3-fortran", "fortran_proposals")
issues = []
for issue in repo.issues(state="all"):
    print(issue.number)
    comments = []
    for comment in issue.comments():
        comments.append(Comment(
            user=str(comment.user.login),
            date=str(comment.created_at),
            text=str(comment.body_text)
            ))
    issues.append(Issue(
            number=issue.number,
            user=str(issue.user.login),
            date=str(issue.created_at),
            title=str(issue.title),
            text=str(issue.body_text),
            comments=comments
            ))
repository = Repository(
        name=str(repo.full_name),
        issues=issues
        )

save_json("data.json", repository)
