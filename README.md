# GitHub Stats

```
$ conda env create -f environment.yml
$ conda activate gh_stats
$ python download_issues.py
$ python stats.py
```

The `download_issues.py` downloads all GitHub issues and saves them into
`data.json`. This script requires you to create a file `account.toml`:
```toml
[github]
user = "your_github_ID"
token = "TOKEN"
```
You can create a GitHub TOKEN at the url: https://github.com/settings/tokens

The script `stats.py` then loads this json and creates a statistics. It does not
require GitHub, your credentials or internet access.
